package ToolsIHM;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class TimeString {
    public TimeString() {
    }

    public static String formatString(LocalTime temps) {
        String stringHeure;
        if (temps.getHour() >= 0 && temps.getHour() < 10) {
            stringHeure = "0" + temps.getHour();
        } else {
            stringHeure = String.valueOf(temps.getHour());
        }

        if (temps.getMinute() >= 0 && temps.getMinute() < 10) {
            stringHeure = stringHeure + ":0" + temps.getMinute();
        } else {
            stringHeure = stringHeure + ":" + temps.getMinute();
        }

        return stringHeure;
    }

    public static String formatString(LocalDateTime temps) {
        String stringHeure;
        if (temps.getHour() >= 0 && temps.getHour() < 10) {
            stringHeure = "0" + temps.getHour();
        } else {
            stringHeure = String.valueOf(temps.getHour());
        }

        if (temps.getMinute() >= 0 && temps.getMinute() < 10) {
            stringHeure = stringHeure + ":0" + temps.getMinute();
        } else {
            stringHeure = stringHeure + ":" + temps.getMinute();
        }

        return stringHeure;
    }

    public static String[] affichageHeures() {
        String[] liste = new String[96];
        LocalTime time = LocalTime.of(0, 0);

        for(int i = 0; i < 96; ++i) {
            liste[i] = formatString(time);
            time = time.plusMinutes(15L);
        }

        return liste;
    }

    public static String roundedCurrentTime() {
        LocalDateTime date = LocalDateTime.now();
        int min = date.getMinute();
        int h = date.getHour();
        if ((min < 0 || min >= 8) && (min < 53 || min > 59)) {
            if (min >= 8 && min < 23) {
                min = 15;
            } else if (min >= 23 && min < 38) {
                min = 30;
            } else if (min >= 38 && min < 53) {
                min = 45;
            } else {
                min = 0;
                date.plusMinutes(60L);
            }
        } else {
            min = 0;
        }

        LocalDateTime newdate = LocalDateTime.of(date.getYear(), date.getMonth(), date.getDayOfMonth(), h, min);
        String[] stringDate = newdate.toString().split("T");
        return stringDate[0] + " " + stringDate[1];
    }

    public static String roundedCurrentDate() {
        LocalDateTime date = LocalDateTime.now();
        int min = date.getMinute();
        int h = date.getHour();
        if ((min < 0 || min >= 8) && (min < 53 || min > 59)) {
            if (min >= 8 && min < 23) {
                min = 15;
            } else if (min >= 23 && min < 38) {
                min = 30;
            } else if (min >= 38 && min < 53) {
                min = 45;
            } else {
                min = 0;
                date.plusMinutes(60L);
            }
        } else {
            min = 0;
        }

        LocalDateTime newdate = LocalDateTime.of(date.getYear(), date.getMonth(), date.getDayOfMonth(), h, min);
        return newdate.toString().split("T")[0] + " " + newdate.toString().split("T")[1];
    }

    public static LocalTime currentTime() {
        LocalTime actualTime = LocalTime.now();
        return actualTime;
    }

    public static boolean compareDates(String date1, String date2) {
        LocalDate daTe1 = LocalDate.of(Integer.parseInt(date1.split("-")[0]), Integer.parseInt(date1.split("-")[1]), Integer.parseInt(date1.split("-")[2]));
        LocalDate daTe2 = LocalDate.of(Integer.parseInt(date1.split("-")[0]), Integer.parseInt(date1.split("-")[1]), Integer.parseInt(date1.split("-")[2]));
        return daTe1.compareTo(daTe2) >= 0 && daTe1.compareTo(daTe2) != 0;
    }

    public static int compareLocalDateTimes(LocalDateTime time1, LocalDateTime time2) {
        return time2.getMinute() + time2.getHour() * 60 - (time1.getMinute() + time1.getHour() * 60);
    }

    public static int compareLocalDateTimeAndLocalTime(LocalDateTime time1, LocalTime time2) {
        return time2.getMinute() + time2.getHour() * 60 - (time1.getMinute() + time1.getHour() * 60);
    }

    public static String toFromatDate(LocalDateTime date) {
        return date.getYear() + "-" + date.getMonthValue() + "-" + date.getDayOfMonth();
    }

    public static LocalDateTime stringToLocalDateTime(String dateToconvert) {
        String[] date = dateToconvert.split(" ")[0].split("-");
        String[] hour = dateToconvert.split(" ")[1].split(":");
        return LocalDateTime.of(Integer.valueOf(date[0]), Integer.valueOf(date[1]), Integer.valueOf(date[2]), Integer.valueOf(hour[0]), Integer.valueOf(hour[1]));
    }

    public static LocalTime stringToLocalTime(String dateToconvert) {
        String[] hour = dateToconvert.split(":");
        return LocalTime.of(Integer.valueOf(hour[0]), Integer.valueOf(hour[1]));
    }

    public static int toPositiveMinutes(LocalTime time) {
        int hour=0;
        if (time.getHour() < 0) {
            hour = -time.getHour();
        } else {
            hour = time.getHour();
        }

        return hour * 60 + time.getMinute();
    }
}
