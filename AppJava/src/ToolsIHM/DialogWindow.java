package ToolsIHM;

import javax.swing.*;
import java.awt.*;

public class DialogWindow {
    public DialogWindow() {
    }

    public static void warning(Component comp, String msg) {
        warning(comp, msg, "Avertissement");
    }

    public static void warning(Component comp, String msg, String title) {
        JOptionPane.showMessageDialog(comp, msg, title, 2);
    }

    public static void error(Component comp, String msg) {
        error(comp, msg, "Erreur");
    }

    public static void error(Component comp, String msg, String title) {
        JOptionPane.showMessageDialog(comp, msg, title, 0);
    }

    public static void message(Component comp, String msg) {
        message(comp, msg, "Information");
    }

    public static void message(Component comp, String msg, String title) {
        JOptionPane.showMessageDialog(comp, msg, title, 1);
    }

    public static boolean yesNo(Component comp, String msg, String title) {
        return JOptionPane.showConfirmDialog(comp, msg, title, 0) == 0;
    }

    public static boolean okCancel(Component comp, String msg, String title) {
        return JOptionPane.showConfirmDialog(comp, msg, title, 2) == 0;
    }

    public static Object input(Component comp, String msg, String title, String initialValue) {
        if (initialValue == null) {
            initialValue = "";
        }

        return JOptionPane.showInputDialog(comp, msg, title, 3, (Icon)null, (Object[])null, initialValue);
    }
}
