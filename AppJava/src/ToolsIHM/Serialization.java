package ToolsIHM;

import java.io.*;

public class Serialization {
    public Serialization() {
    }

    public static Object load(String filename) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(new File(filename));
        ObjectInputStream in = new ObjectInputStream(fis);
        Object object = in.readObject();
        in.close();
        return object;
    }

    public static void save(String filename, Object object) throws IOException {
        FileOutputStream fos = new FileOutputStream(new File(filename));
        ObjectOutputStream out = new ObjectOutputStream(fos);
        out.writeObject(object);
        out.close();
    }
}
