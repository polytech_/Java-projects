package ToolsIHM;

import javax.swing.*;
import java.util.Date;

public class ClockFrame extends JFrame {
    ClockFrame() {
        JLabel label = new JLabel();
        this.getContentPane().add(label);
        Thread t = new ClockFrame.Clock(label);
        t.start();
        System.out.println();
    }

    public static void main(String[] args) {
        ClockFrame frame = new ClockFrame();
        frame.setDefaultCloseOperation(3);
        frame.setSize(200, 60);
        frame.setVisible(true);
    }

    public class Clock extends Thread {
        JLabel labelToUpdate;

        Clock(JLabel label) {
            this.labelToUpdate = label;
        }

        public void run() {
            Date date = new Date();

            while(true) {
                date.setTime(System.currentTimeMillis());
                this.labelToUpdate.setText(date.toString());
                this.labelToUpdate.repaint();

                try {
                    Thread.sleep(1000L);
                } catch (Exception var3) {
                }
            }
        }
    }
}
