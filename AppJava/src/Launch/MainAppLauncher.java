

package Launch;

import ViewsIHM.MainAppIHM;
import java.io.IOException;

public class MainAppLauncher {
    public MainAppLauncher() {
    }

    public static void main(String[] args) {
        MainAppIHM application = null;

        try {
            application = new MainAppIHM();
        } catch (ClassNotFoundException var3) {
            var3.printStackTrace();
        } catch (IOException var4) {
            var4.printStackTrace();
        }

        application.setVisible(true);
    }
}
