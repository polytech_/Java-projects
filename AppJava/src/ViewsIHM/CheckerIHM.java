//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package ViewsIHM;

import CheckerController.checkerClient;
import CheckerController.checkerCommunicationSender;
import ToolsIHM.DialogWindow;
import ToolsIHM.Serialization;
import ToolsIHM.TimeString;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.Serializable;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CheckerIHM extends JFrame implements Serializable {
    private JPanel panel;
    private JButton check;
    private JButton synchronize;
    private JLabel currentDate;
    private JComboBox employees;
    private checkerCommunicationSender dataToSend;

    public CheckerIHM() {
        try {
            this.dataToSend = (checkerCommunicationSender)Serialization.load("./data/saveDonneesEntreprise.ser");
            System.err.println("Loading serial for the checker ..");
        } catch (ClassNotFoundException | IOException | NullPointerException var6) {
            this.dataToSend = new checkerCommunicationSender();
        }

        this.employees = new JComboBox();
        checkerClient pc = null;

        try {
            pc = new checkerClient("localhost", 1337, "GETINFO", this.employees);
        } catch (IOException var5) {
            DialogWindow.error(this, "Sorry, unable to connect to the server !");
            System.exit(0);
        }

        pc.run();
        String[] display = new String[this.employees.getItemCount()];

        for(int i = 0; i < this.employees.getItemCount(); ++i) {
            display[i] = this.employees.getItemAt(i).toString();
        }

        JMenuBar menuBar = new JMenuBar();
        menuBar.setFont(new Font("Trebuchet MS", 1, 12));
        menuBar.setForeground(Color.BLUE);
        JMenu menu = new JMenu("Manual Checking");
        menu.setFont(new Font("Trebuchet MS", 1, 12));
        menu.add(new JMenuItem(new CheckerIHM.manualPointage(display, "Manual checking")));
        menuBar.add(menu);
        this.setJMenuBar(menuBar);
        this.panel = new JPanel();
        this.panel.setForeground(SystemColor.info);
        this.getContentPane().add(this.panel);
        this.panel.setLayout((LayoutManager)null);
        this.check = new JButton("Check in/out");
        this.check.setForeground(SystemColor.textText);
        this.check.setFont(new Font("Trebuchet MS", 1, 13));
        this.check.setBounds(229, 132, 113, 25);
        this.check.addActionListener((e) -> {
            checkerClient pc2 = null;
            String data = "NEW->" + display[this.employees.getSelectedIndex()] + "~" + TimeString.roundedCurrentTime();

            try {
                pc2 = new checkerClient("localhost", 1337, data);
                pc2.run();
                DialogWindow.message(this, "Check-in done successfully for the employee : " + display[this.employees.getSelectedIndex()]);
            } catch (IOException var8) {
                this.dataToSend.addData(data);
                DialogWindow.error(this, "Unable to connect to the server, please try again later !");

                try {
                    Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", this.dataToSend);
                } catch (IOException var7) {
                    var7.printStackTrace();
                }
            }

        });
        this.panel.add(this.check);
        this.synchronize = new JButton("Synchronize");
        this.synchronize.setFont(new Font("Trebuchet MS", 1, 13));
        this.synchronize.setForeground(SystemColor.infoText);
        this.synchronize.setBounds(273, 190, 113, 25);
        this.synchronize.addActionListener((e) -> {
            try {
                for(int i = 0; i < this.dataToSend.getSize(); ++i) {
                    checkerClient pc2 = new checkerClient("localhost", 1337, this.dataToSend.getData(i));
                    pc2.run();
                }

                DialogWindow.message(this, "Synchronization done for " + this.dataToSend.getSize() + " employes");
                this.dataToSend.clear();
                Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", this.dataToSend);
            } catch (IOException var4) {
                DialogWindow.error(this, "Unable to connect to the server, please try again later !");
            }

        });
        this.panel.add(this.synchronize);
        this.currentDate = new JLabel("<html>Today's date is : " + TimeString.roundedCurrentDate().split(" ")[0] + " & time right now is : " + TimeString.currentTime().toString().split(":")[0] + ":" + TimeString.currentTime().toString().split(":")[1] + "<br/>Time rounded up to the nearest fifteen is : " + TimeString.roundedCurrentDate().split(" ")[1] + "<html>", 0);
        this.currentDate.setFont(new Font("Trebuchet MS", 1, 13));
        this.currentDate.setForeground(SystemColor.infoText);
        this.currentDate.setBounds(10, 11, 376, 88);
        this.panel.add(this.currentDate);
        this.employees = new JComboBox(display);
        this.employees.setFont(new Font("Trebuchet MS", 1, 13));
        this.employees.setForeground(SystemColor.infoText);
        this.employees.setBounds(47, 132, 140, 25);
        this.panel.add(this.employees);
        this.setTitle("Checker");
        this.setSize(412, 286);
        this.setLocationRelativeTo((Component)null);
        this.setDefaultCloseOperation(3);
    }

    boolean checkDate(String date) {
        int[] format = new int[3];
        int[] currentDate = new int[3];

        try {
            String[] sFormat = date.split("-");
            String[] sCurrentDate = TimeString.roundedCurrentDate().split(" ")[0].split("-");
            format[0] = Integer.valueOf(sFormat[0]);
            format[1] = Integer.valueOf(sFormat[1]);
            format[2] = Integer.valueOf(sFormat[2]);
            currentDate[0] = Integer.valueOf(sCurrentDate[0]);
            currentDate[1] = Integer.valueOf(sCurrentDate[1]);
            currentDate[2] = Integer.valueOf(sCurrentDate[2]);
        } catch (ArrayIndexOutOfBoundsException | NumberFormatException var7) {
            return false;
        }

        System.err.println(format[0] + "-" + format[1] + "-" + format[2]);
        if (format[0] <= currentDate[0] && format[0] >= 2010) {
            if (format[1] >= 1 && format[1] <= 13 && format[1] <= currentDate[1]) {
                if (format[2] > currentDate[2]) {
                    return false;
                } else if (format[2] >= 1 && format[2] <= 31) {
                    if (format[2] == 31 && format[1] != 1 && format[1] != 3 && format[1] != 5 && format[1] != 7 && format[1] != 8 && format[1] != 10 && format[1] != 12) {
                        return false;
                    } else if (format[2] == 30 && format[1] != 4 && format[1] != 6 && format[1] != 9 && format[1] != 11) {
                        return false;
                    } else if (format[2] == 28 && format[1] == 2 && format[0] % 4 != 0) {
                        return false;
                    } else {
                        return format[2] != 29 || format[1] != 2 || format[0] % 4 != 0;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    class manualPointage extends AbstractAction {
        String[] data;

        public manualPointage(String[] emp, String texte) {
            super(texte);
            this.data = emp;
        }

        public void actionPerformed(ActionEvent e) {
            JFrame fenetre = new JFrame();
            JPanel panel = new JPanel();
            panel.setLayout((LayoutManager)null);
            JComboBox employees = new JComboBox(this.data);
            employees.setBounds(15, 15, 180, 25);
            panel.add(employees);
            JTextField Date = new JTextField(TimeString.roundedCurrentDate().split(" ")[0]);
            Date.setBounds(15, 50, 180, 25);
            panel.add(Date);
            JComboBox heure = new JComboBox(TimeString.affichageHeures());
            heure.setBounds(15, 85, 75, 25);
            panel.add(heure);
            JButton add = new JButton("OK");
            add.setBounds(100, 85, 80, 25);
            add.addActionListener((e2) -> {
                if (CheckerIHM.this.checkDate(Date.getText())) {
                    String formatData = Date.getText() + " " + heure.getItemAt(heure.getSelectedIndex()).toString();
                    String data = "NEW->" + this.data[employees.getSelectedIndex()] + "~" + formatData;

                    try {
                        checkerClient pc2 = new checkerClient("localhost", 1337, data);
                        pc2.run();
                        DialogWindow.message(fenetre, "Check-in done successfully done for the employee : " + this.data[employees.getSelectedIndex()]);
                        fenetre.setVisible(false);
                    } catch (IOException var12) {
                        CheckerIHM.this.dataToSend.addData(data);
                        DialogWindow.error(fenetre, "Unable to connect to the server, please try again later !");
                        fenetre.setVisible(false);

                        try {
                            Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", CheckerIHM.this.dataToSend);
                        } catch (IOException var11) {
                            var11.printStackTrace();
                        }
                    }
                } else {
                    DialogWindow.error(fenetre, "The date entered is not correct, please try again !");
                }

            });
            panel.add(add);
            fenetre.setTitle("More options");
            fenetre.setSize(220, 160);
            fenetre.setLocationRelativeTo((Component)null);
            fenetre.setVisible(true);
            fenetre.getContentPane().add(panel);
        }
    }
}
