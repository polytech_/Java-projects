

package ViewsIHM;

import ControllerDptManagmntMainApp.DeleteOrModifyDepartment;
import ControllerDptManagmntMainApp.addDepartment;
import ControllerTools.JTableModel;
import Model.Company;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EtchedBorder;

public class DepartmentsmanagementIHM extends JFrame {
    private Company entreprise;
    private String[] donnesTitres = new String[]{"The department's name", "The department's manager", "Number of employees"};
    private ArrayList<String> donnes;
    private JTable tableau;
    private JButton bouttonAdd = new JButton("Create/Add a department");

    public DepartmentsmanagementIHM(final Company company) {
        this.setLocationRelativeTo((Component)null);
        this.setTitle("Departments' management");
        this.setSize(600, 300);
        this.entreprise = company;
        this.donnes = company.getDeptInformations();
        final JTableModel tableau1 = new JTableModel(this.donnes, this.donnesTitres);
        this.tableau = new JTable(tableau1);
        JPanel tableBouttonAjouter = new JPanel();
        tableBouttonAjouter.setBackground(SystemColor.info);
        this.bouttonAdd.setFont(new Font("Trebuchet MS", 1, 13));
        this.bouttonAdd.setForeground(Color.BLUE);
        this.bouttonAdd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new addDepartment(company);
                DepartmentsmanagementIHM.this.dispose();
            }
        });
        tableBouttonAjouter.add(this.bouttonAdd);
        this.tableau.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    JTable target = (JTable)e.getSource();
                    int row = target.getSelectedRow();
                    if (row >= company.getDptNb()) {
                        throw new RuntimeException("Error, department not found");
                    }

                    new DeleteOrModifyDepartment(company, company.getDepartment(tableau1.getValueAt(row, 0).toString()));
                    DepartmentsmanagementIHM.this.dispose();
                }

            }
        });
        this.tableau.getColumnModel().getColumn(0).setPreferredWidth(6);
        JScrollPane scrollPane = new JScrollPane(this.tableau);
        scrollPane.setViewportBorder(new EtchedBorder(1, (Color)null, (Color)null));
        this.getContentPane().add(scrollPane, "Center");
        this.getContentPane().add(tableBouttonAjouter, "South");
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        this.setVisible(true);
    }
}
