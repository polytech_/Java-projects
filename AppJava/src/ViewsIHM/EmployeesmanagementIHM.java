

package ViewsIHM;

import ControllerEmpManagmentMainApp.DeleteOrModifyEmployee;
import ControllerEmpManagmentMainApp.addEmployee;
import ControllerTools.JTableModel;
import Model.Company;
import Model.ImportExportCSV;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.filechooser.FileSystemView;

public class EmployeesmanagementIHM extends JFrame {
    private Company entreprise;
    private String[] donnesTitres = new String[]{"Id", "Status", "Name", "Department", "Arrival Time", "Departure Time", "Additionnal Time"};
    private ArrayList<String> donnes;
    private JTable tableau;
    private JButton bouttonAdd = new JButton("Create/Add an employee");
    private final JButton btnNewButton = new JButton("Import CSV");
    private final JButton btnExportCsv = new JButton("Export CSV");
    private final JTextField textField = new JTextField();

    public EmployeesmanagementIHM(final Company company) {
        this.textField.setColumns(10);
        this.setLocationRelativeTo((Component)null);
        this.setTitle("Employees management");
        this.setSize(600, 300);
        this.entreprise = company;
        this.donnes = company.affichageInfosEmployes();
        JTableModel tableau1 = new JTableModel(this.donnes, this.donnesTitres);
        this.tableau = new JTable(tableau1);
        JPanel tableBouttonAjouter = new JPanel();
        tableBouttonAjouter.setBackground(SystemColor.info);
        this.bouttonAdd.setForeground(Color.BLUE);
        this.bouttonAdd.setFont(new Font("Trebuchet MS", 1, 13));
        this.bouttonAdd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new addEmployee(company);
                EmployeesmanagementIHM.this.dispose();
            }
        });
        this.btnNewButton.setFont(new Font("Trebuchet MS", 1, 13));
        this.btnNewButton.setForeground(Color.BLUE);
        this.btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
                jfc.setDialogTitle("Custom button");
                ImportExportCSV impCSV = new ImportExportCSV(company);
                int returnValue = jfc.showDialog((Component)null, "Ok!");
                if (returnValue == 0) {
                    impCSV.ImportCSV(jfc.getSelectedFile().getPath(), company);
                    EmployeesmanagementIHM.this.tableau.repaint();
                    System.out.println(jfc.getSelectedFile().getPath());
                }

            }
        });
        tableBouttonAjouter.add(this.btnNewButton);
        tableBouttonAjouter.add(this.bouttonAdd);
        this.tableau.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    JTable source = (JTable)e.getSource();
                    int row = source.rowAtPoint(e.getPoint());
                    if (row < company.getDptNb()) {
                        new DeleteOrModifyEmployee(company, company.getManager(row));
                    } else {
                        new DeleteOrModifyEmployee(company, company.getEmployee(row - EmployeesmanagementIHM.this.entreprise.getDptNb()));
                    }

                    EmployeesmanagementIHM.this.dispose();
                }

            }
        });
        this.tableau.getColumnModel().getColumn(0).setPreferredWidth(6);
        this.getContentPane().add(new JScrollPane(this.tableau), "Center");
        this.getContentPane().add(tableBouttonAjouter, "South");
        this.btnExportCsv.setFont(new Font("Trebuchet MS", 1, 13));
        this.btnExportCsv.setForeground(Color.BLUE);
        this.btnExportCsv.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                File file = new File("./csvFiles/" + EmployeesmanagementIHM.this.textField.getText() + ".csv");
                ImportExportCSV expCSV = new ImportExportCSV(company);

                try {
                    file.createNewFile();
                    expCSV.ExportCSV(company, file);
                } catch (IOException var5) {
                    System.out.println("Unable to read the file, try again !");
                }

            }
        });
        tableBouttonAjouter.add(this.textField);
        tableBouttonAjouter.add(this.btnExportCsv);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        this.setVisible(true);
    }
}
