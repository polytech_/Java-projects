//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package ViewsIHM;

import ControllerServerCheck.checkerServer;
import Model.Company;
import ToolsIHM.DialogWindow;
import ToolsIHM.Serialization;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class MainAppIHM extends JFrame {
    private Company entreprise;
    private JPanel panelGeneral;
    private JButton empManagement;
    private JButton deptManagement;
    private JButton gestionPointage;
    private JLabel lblWelcomeToThe;

    public MainAppIHM() throws IOException, ClassNotFoundException {
        this.setBackground(Color.WHITE);
        this.setForeground(Color.WHITE);
        this.setFont(UIManager.getFont("Button.font"));

        try {
            this.entreprise = (Company)Serialization.load("./SerialLoad&Save/saveDonneesEntreprise.ser");
            System.err.println("Loading serial for the main application...");
        } catch (IOException var6) {
            try {
                this.entreprise = new Company();
            } catch (IOException var5) {
                DialogWindow.error(this, var5.getMessage());
                System.exit(0);
            }
        }

        this.panelGeneral = new JPanel();
        this.panelGeneral.setBackground(SystemColor.info);
        this.panelGeneral.setForeground(Color.BLACK);
        this.getContentPane().add(this.panelGeneral);
        this.panelGeneral.setLayout((LayoutManager)null);
        this.empManagement = new JButton("Employees' management");
        this.empManagement.setFont(new Font("Trebuchet MS", 3, 13));
        this.empManagement.setForeground(Color.BLUE);
        this.empManagement.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new EmployeesmanagementIHM(MainAppIHM.this.entreprise);
            }
        });
        this.empManagement.setBounds(173, 106, 228, 34);
        this.panelGeneral.add(this.empManagement);
        this.deptManagement = new JButton("Departments' management");
        this.deptManagement.setFont(new Font("Trebuchet MS", 3, 13));
        this.deptManagement.setForeground(new Color(0, 0, 255));
        this.deptManagement.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new DepartmentsmanagementIHM(MainAppIHM.this.entreprise);
            }
        });
        this.deptManagement.setBounds(173, 151, 228, 34);
        this.panelGeneral.add(this.deptManagement);
        this.gestionPointage = new JButton("Cheking history");
        this.gestionPointage.setForeground(Color.BLUE);
        this.gestionPointage.setFont(new Font("Trebuchet MS", 3, 13));
        this.gestionPointage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new CheckerHistoryIHM(MainAppIHM.this.entreprise);
            }
        });
        this.gestionPointage.setBounds(173, 196, 228, 34);
        this.panelGeneral.add(this.gestionPointage);
        JLabel label = new JLabel();
        label.setFont(new Font("Trebuchet MS", 1, 13));
        label.setBounds(173, 57, 228, 23);
        this.panelGeneral.add(label);
        label.setForeground(Color.BLACK);
        Thread t = new MainAppIHM.Clock(label);
        this.lblWelcomeToThe = new JLabel("Welcome to the main app ! ");
        this.lblWelcomeToThe.setFont(new Font("Trebuchet MS", 1, 13));
        this.lblWelcomeToThe.setHorizontalAlignment(0);
        this.lblWelcomeToThe.setForeground(Color.BLUE);
        this.lblWelcomeToThe.setBounds(173, 11, 222, 25);
        this.panelGeneral.add(this.lblWelcomeToThe);
        this.setTitle("Main application");
        this.setSize(600, 300);
        this.setLocationRelativeTo((Component)null);
        this.setDefaultCloseOperation(3);
        checkerServer ps = new checkerServer(this.entreprise, "localhost", 1337);
        ps.open();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        t.start();
    }

    class Clock extends Thread {
        JLabel labelToUpdate;

        Clock(JLabel label) {
            this.labelToUpdate = label;
        }

        public void run() {
            Date date = new Date();

            while(true) {
                date.setTime(System.currentTimeMillis());
                this.labelToUpdate.setText(date.toString());
                this.labelToUpdate.repaint();

                try {
                    Thread.sleep(1000L);
                } catch (Exception var3) {
                }
            }
        }
    }
}
