
package ViewsIHM;

import ControllerTools.JTableModelChecking;
import Model.Company;
import ToolsIHM.TimeString;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class CheckerHistoryIHM extends JFrame {
    JPanel confirmPanel = new JPanel();
    private ArrayList<String> donnees;
    private String[] titres = new String[]{"Id", "Name", "Status", "Departement", "Date", "Checking hour"};
    private String[] modeAffichage = new String[]{"All", "Arrival checking", "Departure cheking"};
    private JTable tableau;

    public CheckerHistoryIHM(Company company) {
        this.setTitle("Checking history");
        this.setLocationRelativeTo((Component)null);
        this.setSize(730, 200);
        this.donnees = new ArrayList();
        JTableModelChecking modelPointage = new JTableModelChecking(this.donnees, this.titres);
        this.tableau = new JTable(modelPointage);
        JComboBox choixEmploye = new JComboBox();
        choixEmploye.setFont(new Font("Trebuchet MS", 1, 13));
        choixEmploye.setForeground(Color.BLUE);
        choixEmploye.addItem("Select an employee");
        String[] emp = company.sendInfos().split("~");

        for(int i = 0; i < emp.length; ++i) {
            choixEmploye.addItem(emp[i]);
        }

        JComboBox choixDepartement = new JComboBox();
        choixDepartement.setForeground(Color.BLUE);
        choixDepartement.setFont(new Font("Trebuchet MS", 1, 13));
        choixDepartement.addItem("Select a department");

        for(int j = 0; j < company.getDepartements().length; ++j) {
            choixDepartement.addItem(company.getDepartements()[j]);
        }

        JComboBox choixPointage = new JComboBox(this.modeAffichage);
        choixPointage.setFont(new Font("Trebuchet MS", 1, 13));
        choixPointage.setForeground(Color.BLUE);
        JButton confirmAllPoint = new JButton("OK");
        confirmAllPoint.setForeground(Color.BLUE);
        confirmAllPoint.setFont(new Font("Trebuchet MS", 1, 13));
        this.confirmPanel.setForeground(SystemColor.info);
        this.confirmPanel.add(choixEmploye);
        this.confirmPanel.add(choixDepartement);
        this.confirmPanel.add(choixPointage);
        this.confirmPanel.add(confirmAllPoint);
        JPanel dateInfo = new JPanel();
        dateInfo.setForeground(SystemColor.info);
        JLabel dateText = new JLabel("From : ");
        JTextField from = new JTextField();
        from.setPreferredSize(new Dimension(70, 20));
        JLabel jusquauText = new JLabel("To : ");
        JTextField to = new JTextField(TimeString.roundedCurrentDate().split(" ")[0]);
        to.setPreferredSize(new Dimension(70, 20));
        dateInfo.add(dateText);
        dateInfo.add(from);
        dateInfo.add(jusquauText);
        dateInfo.add(to);
        this.confirmPanel.add(confirmAllPoint);
        confirmAllPoint.addActionListener((e) -> {
            int filtreOption = 0;
            int filtreTri = 0;
            ArrayList<String> optionD = new ArrayList();
            ArrayList<String> optionT = new ArrayList();
            if (choixPointage.getItemAt(choixPointage.getSelectedIndex()).toString().equals("All")) {
                this.donnees = company.checkingDataGathering(-1);
            } else if (choixPointage.getItemAt(choixPointage.getSelectedIndex()).toString().equals("Arrival checking")) {
                this.donnees = company.checkingDataGathering(0);
            } else {
                this.donnees = company.checkingDataGathering(1);
            }

            if (this.checkDate(from.getText())) {
                optionD.add(from.getText());
                filtreOption = 1;
                if (this.checkDate(to.getText())) {
                    optionD.add(to.getText());
                    filtreOption = 2;
                }
            }

            if (!choixEmploye.getItemAt(choixEmploye.getSelectedIndex()).equals("Select an employee")) {
                optionT.add(choixEmploye.getItemAt(choixEmploye.getSelectedIndex()).toString());
                filtreTri = 1;
                if (!choixDepartement.getItemAt(choixDepartement.getSelectedIndex()).equals("Select a department")) {
                    optionT.add(choixDepartement.getItemAt(choixDepartement.getSelectedIndex()).toString());
                    filtreTri = 2;
                }
            } else if (!choixDepartement.getItemAt(choixDepartement.getSelectedIndex()).equals("Select a department")) {
                optionT.add(choixDepartement.getItemAt(choixDepartement.getSelectedIndex()).toString());
                filtreTri = -1;
            }

            modelPointage.setFiltre(filtreOption, filtreTri, optionD, optionT);
            modelPointage.clear();
            modelPointage.newValue(this.donnees);
        });
        this.getContentPane().add(dateInfo, "North");
        this.getContentPane().add(new JScrollPane(this.tableau), "Center");
        this.getContentPane().add(this.confirmPanel, "South");
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        this.setVisible(true);
    }

    boolean checkDate(String date) {
        int[] format = new int[3];
        int[] currentDate = new int[3];

        try {
            String[] sFormat = date.split("-");
            String[] sCurrentDate = TimeString.currentTime().toString().split(" ")[0].split("-");
            format[0] = Integer.valueOf(sFormat[0]);
            format[1] = Integer.valueOf(sFormat[1]);
            format[2] = Integer.valueOf(sFormat[2]);
            currentDate[0] = Integer.valueOf(sCurrentDate[0]);
            currentDate[1] = Integer.valueOf(sCurrentDate[1]);
            currentDate[2] = Integer.valueOf(sCurrentDate[2]);
        } catch (ArrayIndexOutOfBoundsException | NumberFormatException var7) {
            return false;
        }

        if (format[0] <= currentDate[0] && format[0] >= 2010) {
            if (format[1] >= 1 && format[1] <= 13 && format[1] <= currentDate[1]) {
                if (format[2] > currentDate[2]) {
                    return false;
                } else if (format[2] >= 1 && format[2] <= 31) {
                    if (format[2] == 31 && format[1] != 1 && format[1] != 3 && format[1] != 5 && format[1] != 7 && format[1] != 8 && format[1] != 10 && format[1] != 12) {
                        return false;
                    } else if (format[2] == 30 && format[1] != 4 && format[1] != 6 && format[1] != 9 && format[1] != 11) {
                        return false;
                    } else if (format[2] == 28 && format[1] == 2 && format[0] % 4 != 0) {
                        return false;
                    } else {
                        return format[2] != 29 || format[1] != 2 || format[0] % 4 != 0;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
