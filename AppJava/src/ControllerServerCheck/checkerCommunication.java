package ControllerServerCheck;

import Model.Company;
import Model.Employee;
import Model.Manager;
import ToolsIHM.DialogWindow;
import ToolsIHM.Serialization;
import ToolsIHM.TimeString;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import javax.swing.JFrame;

public class checkerCommunication implements Runnable {
    private Company entreprise;
    private Socket sock;
    private PrintWriter writer = null;
    private BufferedInputStream reader = null;

    public checkerCommunication(Company entreprise, Socket pSock) {
        this.entreprise = entreprise;
        this.sock = pSock;
    }

    public void run() {
        boolean closeConnexion = false;

        while(!this.sock.isClosed()) {
            try {
                String toSend;
                label37: {
                    this.writer = new PrintWriter(this.sock.getOutputStream());
                    this.reader = new BufferedInputStream(this.sock.getInputStream());
                    String response = this.read();
                    String[] sentence = response.split("->");
                    String var5;
                    switch((var5 = sentence[0].toUpperCase()).hashCode()) {
                        case 77184:
                            if (var5.equals("NEW")) {
                                this.GestionCheckIn(sentence[1]);
                                toSend = "Done";
                                break label37;
                            }
                            break;
                        case 64218584:
                            if (var5.equals("CLOSE")) {
                                toSend = "communication is completed !";
                                closeConnexion = true;
                                break label37;
                            }
                            break;
                        case 643491044:
                            if (var5.equals("GETINFO")) {
                                toSend = this.entreprise.sendInfos();
                                break label37;
                            }
                    }

                    toSend = "Unknown command !";
                }

                this.writer.write(toSend);
                this.writer.flush();
                if (closeConnexion) {
                    this.writer = null;
                    this.reader = null;
                    this.sock.close();
                    break;
                }
            } catch (SocketException var6) {
                System.err.println("Connection timed out ! ");
                break;
            } catch (IOException var7) {
                var7.printStackTrace();
            }
        }

    }

    private String read() throws IOException {
        String response = "";
        byte[] b = new byte[4096];
        int stream = this.reader.read(b);
        response = new String(b, 0, stream);
        return response;
    }

    private void GestionCheckIn(String data) {
        Manager man = null;
        Employee emp = null;
        String[] info = data.split("~");
        if (this.entreprise.getManager(info[0]) != null) {
            man = this.entreprise.getManager(info[0]);
            if (man.Check(TimeString.stringToLocalDateTime(info[1]))) {
                System.err.println("The manager : " + man.getName() + " has checked on : " + info[1]);

                try {
                    Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", this.entreprise);
                } catch (IOException var7) {
                    var7.printStackTrace();
                }
            } else {
                System.err.println("The manager : " + man.getName() + " has already checked on the current hour ");
            }
        } else {
            emp = this.entreprise.getEmployee(info[0]);
            if (emp.Check(TimeString.stringToLocalDateTime(info[1]))) {
                System.err.println("The employee : " + emp.getName() + " has checked on : " + info[1]);

                try {
                    Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", this.entreprise);
                } catch (IOException var6) {
                    var6.printStackTrace();
                }
            } else {
                DialogWindow.error(new JFrame(), "The employee : " + emp.getName() + " has already checked on the current hour ");
            }
        }

    }
}