package ControllerServerCheck;


import Model.Company;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class checkerServer {
    private int port = 2345;
    private String host = "127.0.0.1";
    private ServerSocket server = null;
    private boolean isRunning = true;
    private String data;
    private Company entreprise;

    public checkerServer(Company entreprise, String pHost, int pPort) {
        this.entreprise = entreprise;
        this.host = pHost;
        this.port = pPort;

        try {
            this.server = new ServerSocket(this.port, 100, InetAddress.getByName(this.host));
        } catch (IOException var5) {
            var5.printStackTrace();
        }

    }

    public void open() {
        Thread t = new Thread(() -> {
            while(this.isRunning) {
                try {
                    Socket client = this.server.accept();
                    Thread t1 = new Thread(new checkerCommunication(this.entreprise, client));
                    t1.start();
                } catch (IOException var4) {
                    var4.printStackTrace();
                }
            }

            try {
                this.server.close();
            } catch (IOException var3) {
                var3.printStackTrace();
                this.server = null;
            }

        });
        t.start();
    }

    public void close() {
        this.isRunning = false;
    }
}