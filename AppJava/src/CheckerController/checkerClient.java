package CheckerController;

import javax.swing.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class checkerClient implements Runnable {
    private Socket connexion = null;
    private PrintWriter writer = null;
    private BufferedInputStream reader = null;
    private String commande;
    private JComboBox employee;

    public checkerClient(String host, int port, String cmd) throws IOException {
        this.employee = null;
        this.commande = cmd;
        this.connexion = new Socket(host, port);
    }

    public checkerClient(String host, int port, String cmd, JComboBox data) throws IOException {
        this.employee = data;
        this.commande = cmd;
        this.connexion = new Socket(host, port);
    }

    public void run() {
        try {
            Thread.currentThread();
            Thread.sleep(1000L);
        } catch (InterruptedException var4) {
            var4.printStackTrace();
        }

        try {
            this.writer = new PrintWriter(this.connexion.getOutputStream(), true);
            this.reader = new BufferedInputStream(this.connexion.getInputStream());
            this.writer.write(this.commande);
            this.writer.flush();
            String response = this.read();
            if (this.commande == "GETINFO") {
                for(int i = 0; i < this.loadButton(response).length; ++i) {
                    this.employee.addItem(this.loadButton(response)[i]);
                }
            }
        } catch (IOException var5) {
            var5.printStackTrace();
        }

        try {
            Thread.currentThread();
            Thread.sleep(1000L);
        } catch (InterruptedException var3) {
            var3.printStackTrace();
        }

        this.writer.write("CLOSE");
        this.writer.flush();
        this.writer.close();
    }

    private String read() throws IOException {
        String response = "";
        byte[] b = new byte[4096];
        int stream = this.reader.read(b);
        response = new String(b, 0, stream);
        return response;
    }

    private String[] loadButton(String data) {
        return data.split("~");
    }
}
