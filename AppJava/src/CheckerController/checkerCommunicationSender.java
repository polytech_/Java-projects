package CheckerController;

import java.io.Serializable;
import java.util.ArrayList;

public class checkerCommunicationSender implements Serializable {
    private ArrayList<String> data = new ArrayList();

    public checkerCommunicationSender() {
    }

    public void addData(String add) {
        this.data.add(add);
    }

    public String getData(int index) {
        return index < this.data.size() ? (String) this.data.get(index) : "";
    }

    public int getSize() {
        return this.data.size();
    }

    public void clear() {
        this.data.clear();
    }
}