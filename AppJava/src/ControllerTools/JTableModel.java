package ControllerTools;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class JTableModel extends AbstractTableModel {
    private String[] titres;
    private ArrayList<String> donnees;

    public JTableModel(ArrayList<String> donnees1, String[] titres1) {
        this.titres = titres1;
        this.donnees = donnees1;
    }

    public int getRowCount() {
        return this.donnees.size();
    }

    public int getColumnCount() {
        return this.titres.length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        return ((String)this.donnees.get(rowIndex)).split("~")[columnIndex];
    }

    public String getColumnName(int col) {
        return this.titres[col];
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }
}
