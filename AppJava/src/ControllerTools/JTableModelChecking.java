

package ControllerTools;

import ToolsIHM.TimeString;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class JTableModelChecking extends AbstractTableModel {
    private ArrayList<String> donnes;
    private String[] titres;
    private int filtre = 0;
    private int filtreT = 0;
    private ArrayList<String> optionDate;
    private ArrayList<String> optionTri;

    public JTableModelChecking(ArrayList<String> donnesParam, String[] titresParam) {
        this.updateData(donnesParam);
        this.titres = titresParam;
        this.optionDate = new ArrayList();
        this.optionTri = new ArrayList();
    }

    public void setFiltre(int filtreParam, int filtreTParam, ArrayList<String> optionDateParam, ArrayList<String> optionTriParam) {
        this.filtre = filtreParam;
        this.filtreT = filtreTParam;
        this.optionDate = optionDateParam;
        this.optionTri = optionTriParam;
    }

    public int getRowCount() {
        return this.donnes.size();
    }

    public int getColumnCount() {
        return this.titres.length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        return ((String)this.donnes.get(rowIndex)).split("~")[columnIndex];
    }

    public String getColumnName(int col) {
        return this.titres[col];
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void clear() {
        int j = this.getRowCount();
        this.donnes.clear();

        for(int i = 0; i < j; ++i) {
            this.fireTableRowsDeleted(i, i);
        }

    }

    public void newValue(ArrayList<String> nouveau) {
        this.updateData(nouveau);

        for(int i = 0; i < nouveau.size(); ++i) {
            this.fireTableRowsInserted(i, i);
        }

    }

    void updateData(ArrayList<String> nouvellesDonnes) {
        String nameDept;

        if (this.filtre == 0) {
            this.donnes = nouvellesDonnes;
        } else {
            String beforeDate;
            if (this.filtre == 1) {
                this.donnes.clear();
                beforeDate = (String)this.optionDate.get(0);

                for(int i = 0; i < nouvellesDonnes.size(); ++i) {
                    nameDept = ((String)nouvellesDonnes.get(i)).split("~")[4];
                    if (TimeString.compareDates(nameDept, beforeDate) || nameDept.equals(beforeDate)) {
                        this.donnes.add((String)nouvellesDonnes.get(i));
                    }
                }
            } else if (this.filtre == 2) {
                this.donnes.clear();
                beforeDate = (String)this.optionDate.get(0);
                nameDept = (String)this.optionDate.get(1);

                for(int i = 0; i < nouvellesDonnes.size(); ++i) {
                    String currentDate = ((String)nouvellesDonnes.get(i)).split("~")[4];
                    if ((TimeString.compareDates(currentDate, beforeDate) || currentDate.equals(beforeDate)) && !TimeString.compareDates(currentDate, nameDept)) {
                        this.donnes.add((String)nouvellesDonnes.get(i));
                    }
                }
            }
        }

        int i;
        if (this.filtreT >= 1) {
            for(i = 0; i < this.donnes.size(); ++i) {
                nameDept = ((String)this.donnes.get(i)).split("~")[1];
                nameDept = ((String)this.donnes.get(i)).split("~")[3];
                if (!nameDept.equals(this.optionTri.get(0))) {
                    this.donnes.remove(i);
                    --i;
                } else if (this.filtreT == 2 && !nameDept.equals(this.optionTri.get(1))) {
                    this.donnes.remove(i);
                    --i;
                }
            }
        } else if (this.filtreT == -1) {
            for(i = 0; i < this.donnes.size(); ++i) {
                nameDept = ((String)this.donnes.get(i)).split("~")[3];
                if (!nameDept.equals(this.optionTri.get(0))) {
                    this.donnes.remove(i);
                    --i;
                }
            }
        }

    }
}
