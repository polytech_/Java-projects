package Model;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.ArrayList;

public class Department implements Serializable {
    private String name;
    private ArrayList<Employee> employees;
    private Manager manager;

    public Department(String Name) {
        this.name = Name;
        this.employees = new ArrayList();
    }

    public Department(String name, Manager man) {
        this.name = name;
        this.employees = new ArrayList();
        this.manager = man;
    }

    public void setName(String nameDpt) {
        this.name = nameDpt;
    }

    public String toString() {
        String returnString = "Department :{ " + this.getName() + " }" + "\nManager: " + this.manager.getName() + "\nEmployees' list : \n";

        for(int i = 0; i < this.employees.size(); ++i) {
            returnString = returnString + ((Employee)this.employees.get(i)).getName() + "\n";
        }

        return returnString;
    }

    public String getName() {
        return this.name;
    }

    public ArrayList<Employee> getListOfEmployees() {
        return this.employees;
    }

    public void addEmployee(String firstName, String lastName, LocalTime arrH, LocalTime depH) {
        Employee emp = new Employee(firstName, lastName, this, arrH, depH);
        this.employees.add(emp);
        emp.setDepartement(this);
    }

    public void addEmployee(Employee e) {
        this.employees.add(e);
        e.setDepartement(this);
    }

    public Employee getEmployee(int index) {
        return (Employee)this.employees.get(index);
    }

    public Employee getEmployee(String firstN, String lastN) {
        for(int i = 0; i < this.employees.size(); ++i) {
            if (((Employee)this.employees.get(i)).getName().equals(firstN + " " + lastN)) {
                return (Employee)this.employees.get(i);
            }
        }

        return null;
    }

    public int getEmployeesNumber() {
        return this.employees.size();
    }

    public void removeEmployee(Employee e) {
        this.employees.remove(e);
    }

    public ArrayList<Employee> getEmployees() {
        return this.employees;
    }

    public Manager getManager() {
        return this.manager;
    }

    public void setManager(Manager man) {
        this.manager = man;
        this.manager.setIsManaging(man.getIsManaging());
    }
}