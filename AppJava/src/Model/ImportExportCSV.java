package Model;



import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalTime;
import java.util.Iterator;
import java.util.Scanner;

import ToolsIHM.TimeString;

public class ImportExportCSV {
    private String LINE_SEPARATOR = "\n";
    private final String SEPARATOR = ";";
    private String FILE_HEADER = "Id;Status;FirstName;LasttName;Department;ArrTime;DepTime;timeAdd;";
    private FileWriter file;
    private Company company;

    public ImportExportCSV(FileWriter file, Company entreprise) {
        this.file = file;
        this.company = entreprise;
    }

    public void ImportCSV(String filepath, Company company) {
        File file = new File(filepath);

        try {
            Scanner inputStream = new Scanner(file);
            inputStream.next();

            while(inputStream.hasNext()) {
                String data = inputStream.next();
                String[] values = data.split(";");
                if (values[1].equals("Manager")) {
                    LocalTime timeAdd = TimeString.stringToLocalTime(values[7]);
                    LocalTime depTime = TimeString.stringToLocalTime(values[6]);
                    LocalTime arrTime = TimeString.stringToLocalTime(values[5]);
                    if (company.getDepartment(values[4]) != null) {
                        new Manager(values[1], values[2]);
                        company.addDepartement(values[4], values[2], values[3]);
                    } else {
                        Employee emp = new Employee(values[2], values[3], company.getDepartment(values[4]), arrTime, depTime, timeAdd);
                        company.addEmployee(emp, values[4]);
                    }
                }
            }
        } catch (FileNotFoundException var11) {
            JOptionPane.showMessageDialog((Component)null, "File configured unproperly !");
        }

    }

    public ImportExportCSV(Company entreprise) {
        this.company = entreprise;
    }

    public void ExportCSV(Company company, File f) throws IOException {
        FileWriter file = new FileWriter(f, true);
        file.append(this.FILE_HEADER);
        file.append(this.LINE_SEPARATOR);
        Iterator var5 = company.getListOfManagers().iterator();

        while(var5.hasNext()) {
            Manager man = (Manager)var5.next();
            file.append(this.LINE_SEPARATOR);
            file.append(String.valueOf(man.getId()));
            file.append(";");
            if (man instanceof Manager) {
                file.append("Manager");
                file.append(";");
            } else {
                file.append("Employee");
                file.append(";");
            }

            file.append(man.getFirstName());
            file.append(";");
            file.append(man.getFirstName());
            file.append(";");
            if (man.getDepartment() != null) {
                file.append(man.getDepartment().getName());
            } else {
                file.append("None");
            }

            file.append(";");
            file.append(man.getarrivalTime().toString());
            file.append(";");
            file.append(man.getdepartureTime().toString());
            file.append(";");
            file.append(man.getAdditionalTime().toString());
            file.append(";");
        }

        var5 = company.getListOfEmployees().iterator();

        while(var5.hasNext()) {
            Employee emp = (Employee)var5.next();
            file.append(this.LINE_SEPARATOR);
            file.append(String.valueOf(emp.getId()));
            file.append(";");
            if (emp instanceof Employee) {
                file.append("Employee");
                file.append(";");
            } else {
                file.append("Employee");
                file.append(";");
            }

            file.append(emp.getFirstName());
            file.append(";");
            file.append(emp.getFirstName());
            file.append(";");
            if (emp.getDepartment() != null) {
                file.append(emp.getDepartment().getName());
            } else {
                file.append("None");
            }

            file.append(";");
            file.append(emp.getarrivalTime().toString());
            file.append(";");
            file.append(emp.getdepartureTime().toString());
            file.append(";");
            file.append(emp.getAdditionalTime().toString());
            file.append(";");
        }

        file.flush();
        file.close();
        JOptionPane.showMessageDialog((Component)null, "File created and is full !");
    }
}
