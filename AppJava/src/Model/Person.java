package Model;

import java.io.Serializable;

public abstract class Person implements Serializable {
    private String firstName;
    private String lastName;

    public Person() {
        this.firstName = "";
        this.lastName = "";
    }

    public Person(String fName, String lName) {
        this.firstName = fName;
        this.lastName = lName;
    }

    public String getName() {
        return this.getFirstName() + " " + this.getLastName();
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
