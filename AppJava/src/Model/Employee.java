package Model;

import ToolsIHM.TimeString;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;

public class Employee extends Person implements Serializable {

    private Department department;
    private int id;
    private static int cpt;
    private LocalTime arrivalTime;
    private LocalTime departureTime;
    private LocalTime additionalTime;
    private ArrayList<LocalDateTime> arrivalTimes;
    private ArrayList<LocalDateTime> departureTimes;

    public Employee(String firstName, String lastName) {
        super(firstName, lastName);
        this.id = cpt++;
        this.arrivalTime = LocalTime.of(9, 0);
        this.departureTime = LocalTime.of(17, 0);
        this.additionalTime = LocalTime.of(0, 0);
        this.arrivalTimes = new ArrayList();
        this.departureTimes = new ArrayList();
    }

    public Employee(String firstName, String lastName, Department department) {
        super(firstName, lastName);
        this.arrivalTime = LocalTime.of(9, 0);
        this.departureTime = LocalTime.of(17, 0);
        this.additionalTime = LocalTime.of(0, 0);
        this.department = department;
        this.arrivalTimes = new ArrayList();
        this.departureTimes = new ArrayList();
    }

    public Employee(String firstName, String lastName, Department department, LocalTime arrTime, LocalTime depTime) {
        super(firstName, lastName);
        ++this.id;
        this.additionalTime = LocalTime.of(0, 0);
        this.department = department;
        this.arrivalTime = arrTime;
        this.departureTime = depTime;
        this.arrivalTimes = new ArrayList();
        this.departureTimes = new ArrayList();
    }

    public Employee(String firstName, String lastName, Department department, LocalTime arrTime, LocalTime depTime, LocalTime addTime) {
        super(firstName, lastName);
        ++this.id;
        this.additionalTime = LocalTime.of(0, 0);
        this.department = department;
        this.arrivalTime = arrTime;
        this.departureTime = depTime;
        this.additionalTime = addTime;
        this.arrivalTimes = new ArrayList();
        this.departureTimes = new ArrayList();
    }

    public String toString() {
        return "Employé : " + this.getId() + "; Nom : " + this.getName() + "; Département : " + this.getDepartment().getName() + "; Heure d'arrivée : " + this.getarrivalTime() + "; Heure de départ : " + this.getdepartureTime();
    }

    public void setDepartement(Department department) {
        this.department = department;
    }

    public int getId() {
        return this.id;
    }

    public void setarrivalTime(LocalTime narrivalTime) {
        this.arrivalTime = narrivalTime;
    }

    public void setdepartureTime(LocalTime ndepartureTime) {
        this.departureTime = ndepartureTime;
    }

    public LocalTime getarrivalTime() {
        return this.arrivalTime;
    }

    public Department getDepartment() {
        return this.department;
    }

    public LocalTime getdepartureTime() {
        return this.departureTime;
    }

    public LocalTime getAdditionalTime() {
        return this.additionalTime;
    }

    public ArrayList<LocalDateTime> getArrivalTimes() {
        return this.arrivalTimes;
    }

    public ArrayList<LocalDateTime> getDepartureTimes() {
        return this.departureTimes;
    }

    public void setId(int idToSet) {
        this.id = idToSet;
    }

    public boolean Check(LocalDateTime hourOfCheck) {
        int Difference;
        if (this.departureTimes.size() == this.arrivalTimes.size()) {
            if (this.departureTimes.size() != 0 && TimeString.toFromatDate(hourOfCheck).compareTo(TimeString.toFromatDate((LocalDateTime)this.departureTimes.get(this.departureTimes.size() - 1))) == 0) {
                return false;
            } else {
                Difference = TimeString.compareLocalDateTimeAndLocalTime(hourOfCheck, this.arrivalTime);
                if (Difference < 0 && TimeString.toPositiveMinutes(this.additionalTime) <= -Difference) {
                    this.additionalTime = LocalTime.of(0, 0);
                } else {
                    this.additionalTime.plusMinutes((long)Difference);
                }

                this.arrivalTimes.add(hourOfCheck);
                return true;
            }
        } else {
            Difference = TimeString.compareLocalDateTimes((LocalDateTime)this.arrivalTimes.get(this.arrivalTimes.size() - 1), hourOfCheck);
            if (Difference < 60) {
                return false;
            } else {
                Difference = TimeString.compareLocalDateTimeAndLocalTime(hourOfCheck, this.departureTime);
                if (Difference != 0) {
                    this.additionalTime.plusMinutes((long)(-Difference));
                }

                this.departureTimes.add(hourOfCheck);
                return true;
            }
        }
    }
}
