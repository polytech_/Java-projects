package Model;

import java.io.Serializable;
import java.time.LocalTime;

public class Manager extends Employee implements Serializable {

    boolean IsManaging;

    public Manager(Employee emp) {
        super(emp.getFirstName(), emp.getLastName(), emp.getDepartment());
        this.setId(emp.getId());
        this.setIsManaging(true);
    }

    public Manager(String firstName, String lastName, Department dep) {
        super(firstName, lastName);
        this.setDepartement(dep);
    }

    public Manager(String firstName, String lastName) {
        super(firstName, lastName);
    }

    public Manager(String firstName, String lastName, Department department, LocalTime arrTime, LocalTime depTime) {
        super(firstName, lastName, department, arrTime, depTime);
    }

    public String toString() {
        String list = "Manager : " + this.getId() + "; Name : " + this.getName() + "; Department : " + this.getDepartment().getName() + "; Arrival time : " + this.getarrivalTime() + "; Departure time : " + this.getdepartureTime();
        if (this.IsManaging) {
            list = list + " & he's managing the department : " + this.getDpt().getName();
        } else {
            list = list + " & he's not managing a department";
        }

        return list;
    }

    public void setDepartement(Department department) {
        this.IsManaging = true;
        super.setDepartement(department);
    }

    public Department getDpt() {
        return super.getDepartment();
    }

    public boolean getIsManaging() {
        return this.IsManaging;
    }

    public void setIsManaging(boolean pIsManaging) {
        this.IsManaging = pIsManaging;
    }
}
