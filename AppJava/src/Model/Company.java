package Model;


import ToolsIHM.Serialization;
import ToolsIHM.TimeString;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.Iterator;
import java.util.Properties;

public class Company implements Serializable {
    private String name;
    private Boss boss;
    private ArrayList<Manager> managers;
    private ArrayList<Department> departments;
    private ArrayList<Employee> employees;



    public Company() throws InvalidPropertiesFormatException, FileNotFoundException, IOException {
        Properties initialisation_XML = new Properties();
        initialisation_XML.loadFromXML(new FileInputStream("./SerialLoad&Save/initialisation_XML.xml"));
        if (initialisation_XML.getProperty("nameCompany") == null) {
            throw new RuntimeException("Le fichier initialisation_XML.xml est mal configuré !");
        } else {
            this.name = initialisation_XML.getProperty("nameCompany");
            this.managers = new ArrayList<Manager>();
            this.employees = new ArrayList<Employee>();
            this.departments = new ArrayList<Department>();
            if (initialisation_XML.getProperty("NameBoss") == null) {
                throw new RuntimeException("Le fichier initialisation_XML.xml est mal configuré !");
            } else {
                this.boss = new Boss(initialisation_XML.getProperty("NameBoss").split(" ")[0], initialisation_XML.getProperty("NameBoss").split(" ")[1]);
                if (initialisation_XML.getProperty("nbDepartements") == null) {
                    throw new RuntimeException("Le fichier initialisation_XML.xml est mal configuré !");
                } else {
                    int j;
                    for(j = 0; j < Integer.parseInt(initialisation_XML.getProperty("nbDepartements")); ++j) {
                        if (initialisation_XML.getProperty("departement" + j) == null || initialisation_XML.getProperty("ManagerName" + j) == null) {
                            throw new RuntimeException("Le fichier initialisation_XML.xml est mal configuré !");
                        }

                        this.addDepartement(initialisation_XML.getProperty("departement" + j), initialisation_XML.getProperty("ManagerName" + j).split(" ")[0], initialisation_XML.getProperty("ManagerName" + j).split(" ")[1]);
                    }

                    for(j = 0; j < Integer.parseInt(initialisation_XML.getProperty("nbDepartements")); ++j) {
                        for(int i = 1; i <= Integer.parseInt(initialisation_XML.getProperty("nBEmpsDpt" + j)); ++i) {
                            if (initialisation_XML.getProperty("employeeName" + i + j) == null) {
                                throw new RuntimeException("Le fichier initialisation_XML.xml est mal configuré !");
                            }

                            this.addEmployee(initialisation_XML.getProperty("employeeName" + i + j).split(" ")[0], initialisation_XML.getProperty("employeeName" + i + j).split(" ")[1], initialisation_XML.getProperty("departement" + j), LocalTime.of(9, 0), LocalTime.of(17, 0));
                        }
                    }

                }
            }
        }
    }

    public Company(String nom, Boss b) {
        this.name = nom;
        this.boss = b;
        this.managers = new ArrayList<Manager>();
        this.employees = new ArrayList<Employee>();
        this.departments = new ArrayList<Department>();
    }

    public static Company generateComany() {
        Boss patron = new Boss("Othmane", "Allamou");
        Company entreprise = new Company("ASUS", patron);
        Employee emp1 = new Employee("Thomas", "Mendes");
        Employee emp2 = new Employee("Tariq", "Chaariat");
        Employee emp3 = new Employee("Aimeric", "Martin");
        Employee emp4 = new Employee("Alichan", "Zepou");
        Employee emp5 = new Employee("Leo", "Pinot");
        Employee emp6 = new Employee("Alex", "Ajinça");
        Employee emp7 = new Employee("Richard", "Benoit");
        Employee emp8 = new Employee("Pierre", "Dupont");
        Employee emp9 = new Employee("André", "Legrand");
        entreprise.addDepartement("RH");
        entreprise.addEmployee(emp1, "RH");
        entreprise.addEmployee(emp2, "RH");
        entreprise.addEmployee(emp7, "RH");
        entreprise.setManager(entreprise.getDepartment(0).getName(), "Richard", "Benoit");
        entreprise.addDepartement("Comptablilité");
        entreprise.addEmployee(emp3, "Comptablilité");
        entreprise.addEmployee(emp4, "Comptablilité");
        entreprise.addEmployee(emp8, "Comptablilité");
        entreprise.setManager(entreprise.getDepartment(1).getName(), "Pierre", "Dupont");
        entreprise.addDepartement("Média et communication");
        entreprise.addEmployee(emp5, "Média et communication");
        entreprise.addEmployee(emp6, "Média et communication");
        entreprise.addEmployee(emp9, "Média et communication");
        entreprise.setManager(entreprise.getDepartment(2).getName(), "André", "Legrand");
        return entreprise;
    }

    public String toString() {
        String list = "Entreprise : " + this.name + "\n";
        list = list + "dirigée par: " + this.boss + "\n";
        list = list + "Liste des departements: \n";

        Department dep;
        Iterator var3;
        for(var3 = this.departments.iterator(); var3.hasNext(); list = list + dep.getName() + "\n") {
            dep = (Department)var3.next();
        }

        list = list + "Liste des managers: \n";

        Employee emp;
        for(var3 = this.employees.iterator(); var3.hasNext(); list = list + emp.getName() + "\n") {
            emp = (Employee)var3.next();
        }

        list = list + "Liste des employés: \n";

        for(int i = 0; i < this.departments.size(); ++i) {
            for(int j = 0; i < ((Department)this.departments.get(i)).getEmployeesNumber(); ++j) {
                list = list + ((Department)this.departments.get(i)).getEmployee(j).getName();
            }
        }

        return list;
    }

    public void addDepartement(String name) {
        Department dep = new Department(name);
        if (this.getDepartment(dep) == null) {
            this.departments.add(dep);

            try {
                Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", this);
            } catch (IOException var4) {
                var4.printStackTrace();
            }

        } else {
            throw new RuntimeException("The department already exists !");
        }
    }

    public void addDepartement(String nameDepartement, String firstNameManager, String lastNameManager) {
        if (this.getDepartment(nameDepartement) == null) {
            Department departement = new Department(nameDepartement);
            if (this.getDepartment(departement) == null) {
                this.departments.add(departement);
                Department dept = this.getDepartment(nameDepartement);
                Manager man = new Manager(firstNameManager, lastNameManager, dept);
                man.setIsManaging(true);
                dept.setManager(man);
                this.managers.add(man);

                try {
                    Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", this);
                } catch (IOException var8) {
                    var8.printStackTrace();
                }

            } else {
                throw new RuntimeException("The department already exists !");
            }
        } else {
            throw new RuntimeException("The department already exist !");
        }
    }

    public Department getDepartment(String name) {
        Iterator var3 = this.departments.iterator();

        while(var3.hasNext()) {
            Department d = (Department)var3.next();
            if (d.getName().equals(name)) {
                return d;
            }
        }

        return null;
    }

    public void removeDepartement(String name) {
        if (this.getDepartment(name) == null) {
            throw new RuntimeException("The department does not exist !");
        } else {
            Department dep = this.getDepartment(name);

            for(int i = 0; i < dep.getEmployeesNumber(); ++i) {
                dep.removeEmployee((Employee)dep.getEmployees().get(i));
            }

            this.departments.remove(dep);

            try {
                Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", this);
            } catch (IOException var4) {
                var4.printStackTrace();
            }

        }
    }

    public Department getDepartment(Department dep) {
        Iterator var3 = this.departments.iterator();

        while(var3.hasNext()) {
            Department d = (Department)var3.next();
            if (d.equals(dep)) {
                return d;
            }
        }

        return null;
    }

    public Department getDepartment(int id) {
        return id > this.departments.size() ? null : (Department)this.departments.get(id);
    }

    public String[] getDepartements() {
        String[] list = new String[this.departments.size()];
        int i = 0;

        for(Iterator var4 = this.departments.iterator(); var4.hasNext(); ++i) {
            Department sd = (Department)var4.next();
            list[i] = sd.getName();
        }

        return list;
    }

    public ArrayList<String> getDeptInformations() {
        ArrayList<String> infos = new ArrayList<String>();

        for(int i = 0; i < this.departments.size(); ++i) {
            String temp = "";
            temp = temp + ((Department)this.departments.get(i)).getName() + "~";
            temp = temp + ((Department)this.departments.get(i)).getManager().getName() + "~";
            temp = temp + String.valueOf(((Department)this.departments.get(i)).getEmployeesNumber()) + "~";
            infos.add(temp);
        }

        return infos;
    }

    public void setManager(String nameDpt, String firstNameManager, String lastNameManager) {
        Department dep = this.getDepartment(nameDpt);
        Employee emp = dep.getEmployee(firstNameManager, lastNameManager);
        Manager man = new Manager(emp);
        dep.setManager(man);
        this.removeEmployee(emp);
        this.managers.add(man);

        try {
            Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", this);
        } catch (IOException var8) {
            var8.printStackTrace();
        }

    }

    public Manager getManager(int index) {
        return (Manager)this.managers.get(index);
    }

    public String[] getEmployee() {
        String[] listEmployee = new String[this.employees.size()];

        for(int i = 0; i < this.employees.size(); ++i) {
            listEmployee[i] = ((Employee)this.employees.get(i)).getName();
        }

        return listEmployee;
    }

    public void addEmployee(Employee emp, String departement) {
        if (this.getEmployee(emp.getName()) == null && this.getDepartment(departement) != null) {
            this.getDepartment(departement).addEmployee(emp);
            this.employees.add(emp);

            try {
                Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", this);
            } catch (IOException var4) {
                var4.printStackTrace();
            }

        } else {
            throw new RuntimeException("The employee already exists or the department affected doest not exists !");
        }
    }

    public Employee getEmployee(int index) {
        if (index < this.employees.size()) {
            return (Employee)this.employees.get(index);
        } else {
            throw new RuntimeException("Employee not found !");
        }
    }

    public Employee getEmployee(String Name) {
        for(int i = 0; i < this.employees.size(); ++i) {
            if (((Employee)this.employees.get(i)).getName().equals(Name)) {
                return (Employee)this.employees.get(i);
            }
        }

        return null;
    }

    public void addEmployee(String firstName, String lastName, String departement, LocalTime arrH, LocalTime depH) {
        if (this.getDepartment(departement) != null) {
            this.getDepartment(departement).addEmployee(firstName, lastName, arrH, depH);
            Employee emp = this.getDepartment(departement).getEmployee(this.getDepartment(departement).getEmployeesNumber() - 1);
            emp.setId(this.employees.size() + this.managers.size());
            this.employees.add(emp);

            try {
                Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", this);
            } catch (IOException var8) {
                var8.printStackTrace();
            }
        }

    }

    public void removeEmployee(Employee employee) {
        employee.getDepartment().removeEmployee(employee);
        this.employees.remove(employee);

        try {
            Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", this);
        } catch (IOException var3) {
            var3.printStackTrace();
        }

    }

    public ArrayList<Employee> getListOfEmployees() {
        return this.employees;
    }

    public int getEmployeesNb() {
        return this.employees.size();
    }

    public int getDptNb() {
        return this.departments.size();
    }

    public Manager getManager(String Name) {
        for(int i = 0; i < this.managers.size(); ++i) {
            if (((Manager)this.managers.get(i)).getName().equals(Name)) {
                return (Manager)this.managers.get(i);
            }
        }

        return null;
    }

    public ArrayList<Manager> getListOfManagers() {
        return this.managers;
    }

    public ArrayList<String> affichageInfosEmployes() {
        String temp1 = "";
        ArrayList<String> infos = new ArrayList<String>();

        for(int i = 0; i < this.managers.size(); ++i) {
            String tmpString = "";
            tmpString = tmpString + String.valueOf(((Manager)this.managers.get(i)).getId()) + "~";
            tmpString = tmpString + "Manager~";
            tmpString = tmpString + ((Manager)this.managers.get(i)).getName() + "~";
            tmpString = tmpString + ((Manager)this.managers.get(i)).getDepartment().getName() + "~";
            tmpString = tmpString + TimeString.formatString(((Manager)this.managers.get(i)).getarrivalTime()) + "~";
            tmpString = tmpString + TimeString.formatString(((Manager)this.managers.get(i)).getdepartureTime()) + "~";
            tmpString = tmpString + TimeString.formatString(((Manager)this.managers.get(i)).getAdditionalTime()) + "~";
            infos.add(tmpString);
        }

        Iterator var5 = this.employees.iterator();

        while(var5.hasNext()) {
            Employee e = (Employee)var5.next();
            String temp2 = "";
            temp2 = temp2 + String.valueOf(e.getId()) + "~";
            temp2 = temp2 + "Employe~";
            temp2 = temp2 + e.getName() + "~";
            temp2 = temp2 + e.getDepartment().getName() + "~";
            temp2 = temp2 + TimeString.formatString(e.getarrivalTime()) + "~";
            temp2 = temp2 + TimeString.formatString(e.getdepartureTime()) + "~";
            temp2 = temp2 + TimeString.formatString(e.getAdditionalTime()) + "~";
            infos.add(temp2);
        }

        return infos;
    }

    public String sendInfos() {
        String liste = "";

        Employee emp;
        for(Iterator var3 = this.employees.iterator(); var3.hasNext(); liste = emp.getName() + "~" + liste) {
            emp = (Employee)var3.next();
        }

        for(int i = 0; i < this.managers.size(); ++i) {
            liste = ((Manager)this.managers.get(i)).getName() + "~" + liste;
        }

        return liste;
    }

    public ArrayList<String> checkingDataGathering(int option) {
        ArrayList<String> listeInfos = new ArrayList<String>();

        for(int i = 0; i < this.managers.size(); ++i) {
            String data = String.valueOf(((Manager)this.managers.get(i)).getId()) + "~" + ((Manager)this.managers.get(i)).getName() + "~Manager~" + ((Manager)this.managers.get(i)).getDepartment().getName();

            int j;
            for(j = 0; j < ((Manager)this.managers.get(i)).getArrivalTimes().size(); ++j) {
                if (option != 1) {
                    listeInfos.add(data + "~" + TimeString.toFromatDate((LocalDateTime)((Manager)this.managers.get(i)).getArrivalTimes().get(j)) + "~" + TimeString.formatString((LocalDateTime)((Manager)this.managers.get(i)).getArrivalTimes().get(j)));
                }
            }

            for(j = 0; j < ((Manager)this.managers.get(i)).getDepartureTimes().size(); ++j) {
                if (option != 0) {
                    listeInfos.add(data + "~" + TimeString.toFromatDate((LocalDateTime)((Manager)this.managers.get(i)).getDepartureTimes().get(j)) + "~" + TimeString.formatString((LocalDateTime)((Manager)this.managers.get(i)).getDepartureTimes().get(j)));
                }
            }
        }

        Iterator var8 = this.employees.iterator();

        while(var8.hasNext()) {
            Employee e = (Employee)var8.next();
            String data = String.valueOf(e.getId()) + "~" + e.getName() + "~Employee~" + e.getDepartment().getName();

            int j;
            for(j = 0; j < e.getArrivalTimes().size(); ++j) {
                if (option != 1) {
                    listeInfos.add(data + "~" + TimeString.toFromatDate((LocalDateTime)e.getArrivalTimes().get(j)) + "~" + TimeString.formatString((LocalDateTime)e.getArrivalTimes().get(j)));
                }
            }

            for(j = 0; j < e.getDepartureTimes().size(); ++j) {
                if (option != 0) {
                    listeInfos.add(data + "~" + TimeString.toFromatDate((LocalDateTime)e.getDepartureTimes().get(j)) + "~" + TimeString.formatString((LocalDateTime)e.getDepartureTimes().get(j)));
                }
            }
        }

        return listeInfos;
    }
}