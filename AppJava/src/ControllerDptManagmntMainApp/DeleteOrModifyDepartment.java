package ControllerDptManagmntMainApp;

import Model.Company;
import Model.Department;
import Model.Employee;
import ToolsIHM.DialogWindow;
import ToolsIHM.Serialization;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;





public class DeleteOrModifyDepartment extends JFrame implements ActionListener {
    private JLabel fieldManager;
    private JComboBox managerSaisi;
    private JLabel fieldNom;
    private JTextField nomSaisi;
    private JButton boutton;
    private JButton delete;
    private Company company;
    private Department department;

    public DeleteOrModifyDepartment(Company entreprise, Department dep) {
        this.setTitle("Delete or modify a department");
        this.company = entreprise;
        this.department = dep;
        this.initialize();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
    }

    private void initialize() {
        this.fieldNom = new JLabel("Department's name :");
        this.fieldNom.setBounds(50, 30, 150, 20);
        this.nomSaisi = new JTextField(this.department.getName());
        this.nomSaisi.setBounds(50, 50, 150, 20);
        this.fieldManager = new JLabel("Department's manager :");
        this.fieldManager.setBounds(50, 70, 150, 20);
        this.managerSaisi = new JComboBox(this.company.getEmployee());
        this.managerSaisi.addItem(this.department.getManager().getName());
        this.managerSaisi.setSelectedItem(this.managerSaisi.getItemCount());
        this.managerSaisi.setBounds(50, 90, 150, 20);
        this.boutton = new JButton("Modify");
        this.boutton.setBounds(50, 130, 95, 30);
        this.boutton.addActionListener(this);
        this.delete = new JButton("Remove");
        this.delete.setBounds(50, 170, 95, 30);
        this.delete.addActionListener((e) -> {
            if (DialogWindow.okCancel(this, "Are you sure about removing this department ?", "Removing ...")) {
                this.company.removeDepartement(this.department.getName());
                this.setVisible(false);
            }

        });
        this.getContentPane().add(this.boutton);
        this.getContentPane().add(this.delete);
        this.getContentPane().add(this.fieldNom);
        this.getContentPane().add(this.nomSaisi);
        this.getContentPane().add(this.fieldManager);
        this.getContentPane().add(this.managerSaisi);
        this.setSize(369, 274);
        this.getContentPane().setLayout((LayoutManager)null);
        this.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        String valeurNomSaisi = this.nomSaisi.getText();
        if (valeurNomSaisi.isEmpty()) {
            DialogWindow.error(this, "Incorrect name or empty field !");
        } else {
            this.department.setName(valeurNomSaisi);
            if (!this.department.getManager().getName().equals(this.managerSaisi.getItemAt(this.managerSaisi.getSelectedIndex()).toString())) {
                Employee emp = this.company.getEmployee(this.managerSaisi.getItemAt(this.managerSaisi.getSelectedIndex()).toString());
                this.company.setManager(valeurNomSaisi, emp.getFirstName(), emp.getLastName());

                try {
                    Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", this.company);
                } catch (IOException var5) {
                    var5.printStackTrace();
                }
            }

            this.setVisible(false);
            DialogWindow.message(this, "Modifie avec succes !");
        }

    }
}
