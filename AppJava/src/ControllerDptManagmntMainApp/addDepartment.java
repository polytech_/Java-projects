package ControllerDptManagmntMainApp;

import Model.Company;
import Model.Employee;
import ToolsIHM.DialogWindow;
import ToolsIHM.Serialization;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


public class addDepartment extends JFrame implements ActionListener {
    private JLabel fieldManager;
    private JComboBox managerSaisi;
    private JLabel fieldNom;
    private JTextField nomSaisi;
    private JButton boutton;
    private Company entreprise;

    public addDepartment(Company company) {
        this.setTitle("Add a department");
        this.entreprise = company;
        this.initialize();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
    }

    private void initialize() {
        this.fieldNom = new JLabel("Department's name :");
        this.fieldNom.setBounds(50, 30, 150, 20);
        this.nomSaisi = new JTextField();
        this.nomSaisi.setBounds(50, 50, 150, 20);
        this.fieldManager = new JLabel("Department's management :");
        this.fieldManager.setBounds(50, 70, 150, 20);
        this.managerSaisi = new JComboBox(this.entreprise.getEmployee());
        this.managerSaisi.setBounds(50, 90, 150, 20);
        this.boutton = new JButton("Add");
        this.boutton.setBounds(50, 130, 95, 30);
        this.boutton.addActionListener(this);
        this.getContentPane().add(this.boutton);
        this.getContentPane().add(this.fieldNom);
        this.getContentPane().add(this.nomSaisi);
        this.getContentPane().add(this.fieldManager);
        this.getContentPane().add(this.managerSaisi);
        this.setSize(327, 227);
        this.getContentPane().setLayout((LayoutManager)null);
        this.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        String nomSaisi1 = this.nomSaisi.getText();
        if (nomSaisi1.isEmpty()) {
            DialogWindow.error(this, "Incorrect name or empty field !");
        } else {
            Employee managerDuDep = this.entreprise.getEmployee(this.managerSaisi.getItemAt(this.managerSaisi.getSelectedIndex()).toString());
            this.entreprise.addDepartement(nomSaisi1, managerDuDep.getFirstName(), managerDuDep.getLastName());
            DialogWindow.message(this, "Added successfully !");
            this.setVisible(false);

            try {
                Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", this.entreprise);
            } catch (IOException var5) {
                var5.printStackTrace();
            }
        }

    }
}
