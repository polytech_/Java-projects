package ControllerEmpManagmentMainApp;

import Model.Company;
import ToolsIHM.DialogWindow;
import ToolsIHM.Serialization;
import ToolsIHM.TimeString;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class addEmployee extends JFrame implements ActionListener {
    private JButton bouttonAdd;
    private Company entreprise;
    private JLabel prenom;
    private JTextField prenomSaisi;
    private JLabel nom;
    private JTextField nomSaisi;
    private JLabel departement;
    private JComboBox departements;
    private JLabel heureArrivee;
    private JLabel heureDepart;
    private JComboBox heureArriveChoix;
    private JComboBox heureDepartChoix;
    private JComboBox comboBox;

    public addEmployee(Company company) {
        this.setTitle("Add en employee");
        this.entreprise = company;
        this.initialize();
        this.setSize(500, 300);
        this.getContentPane().setLayout((LayoutManager)null);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        this.setVisible(true);
    }

    private void initialize() {
        this.nom = new JLabel("Employee's last name :");
        this.nom.setBounds(50, 30, 150, 20);
        this.nomSaisi = new JTextField();
        this.nomSaisi.setBounds(50, 50, 150, 20);
        this.prenom = new JLabel("Employee's first name :");
        this.prenom.setBounds(245, 30, 150, 20);
        this.prenomSaisi = new JTextField();
        this.prenomSaisi.setBounds(245, 50, 150, 20);
        this.departement = new JLabel("Employee's department :");
        this.departement.setBounds(86, 156, 150, 20);
        this.departements = new JComboBox(this.entreprise.getDepartements());
        this.departements.setBounds(231, 156, 150, 20);
        this.heureArrivee = new JLabel("Employee's arrival time :");
        this.heureArrivee.setBounds(50, 90, 150, 20);
        this.heureArriveChoix = new JComboBox(TimeString.affichageHeures());
        this.heureArriveChoix.setBounds(50, 110, 150, 20);
        this.heureArriveChoix.setSelectedItem("09:00");
        this.heureDepart = new JLabel("Employee's departure time :");
        this.heureDepart.setBounds(245, 90, 150, 20);
        this.heureDepartChoix = new JComboBox(TimeString.affichageHeures());
        this.heureDepartChoix.setBounds(245, 110, 150, 20);
        this.heureDepartChoix.setSelectedItem("17:00");
        this.bouttonAdd = new JButton("Add");
        this.bouttonAdd.setBounds(191, 220, 95, 30);
        this.bouttonAdd.addActionListener(this);
        this.getContentPane().add(this.bouttonAdd);
        this.getContentPane().add(this.heureArrivee);
        this.getContentPane().add(this.heureDepart);
        this.getContentPane().add(this.heureArriveChoix);
        this.getContentPane().add(this.heureDepartChoix);
        this.getContentPane().add(this.nom);
        this.getContentPane().add(this.departement);
        this.getContentPane().add(this.departements);
        this.getContentPane().add(this.prenom);
        this.getContentPane().add(this.nomSaisi);
        this.getContentPane().add(this.prenomSaisi);
    }

    public void actionPerformed(ActionEvent e) {
        String nomAsaisir = this.nomSaisi.getText();
        String prenomAsaisir = this.prenomSaisi.getText();
        int choixHeureArrivee = this.heureArriveChoix.getSelectedIndex();
        int choixHeureDepart = this.heureDepartChoix.getSelectedIndex();
        String[] heuresArrivee = this.heureArriveChoix.getItemAt(choixHeureArrivee).toString().split(":");
        String[] heuresDepart = this.heureDepartChoix.getItemAt(choixHeureDepart).toString().split(":");
        LocalTime testHeureArrivee = LocalTime.of(Integer.parseInt(heuresArrivee[0]), Integer.parseInt(heuresArrivee[1]));
        LocalTime testHeureDepart = LocalTime.of(Integer.parseInt(heuresDepart[0]), Integer.parseInt(heuresDepart[1]));
        long DureedeTravail = ChronoUnit.MINUTES.between(testHeureArrivee, testHeureDepart);
        if (DureedeTravail < 0L) {
            DureedeTravail *= -1L;
        }

        if (!prenomAsaisir.isEmpty() && !prenomAsaisir.contains("?") && !prenomAsaisir.contains(";") && !prenomAsaisir.contains(",") && !prenomAsaisir.contains("/") && !prenomAsaisir.contains("*") && !prenomAsaisir.contains("-") && !prenomAsaisir.contains("+") && !prenomAsaisir.contains(" ") && !prenomAsaisir.contains("(") && !prenomAsaisir.contains(")") && !prenomAsaisir.contains("<") && !prenomAsaisir.contains(">")) {
            if (!nomAsaisir.isEmpty() && !nomAsaisir.contains("?") && !nomAsaisir.contains(";") && !nomAsaisir.contains(",") && !nomAsaisir.contains("/") && !nomAsaisir.contains("*") && !nomAsaisir.contains("-") && !nomAsaisir.contains("+") && !nomAsaisir.contains(" ") && !nomAsaisir.contains("(") && !nomAsaisir.contains(")") && !nomAsaisir.contains("<") && !nomAsaisir.contains(">")) {
                if (DureedeTravail > 720L) {
                    DialogWindow.error(this, "The employee cannot work more than 12 hours a day !");
                } else {
                    this.entreprise.addEmployee(nomAsaisir, prenomAsaisir, this.departements.getItemAt(this.departements.getSelectedIndex()).toString(), testHeureArrivee, testHeureDepart);
                    DialogWindow.message(this, "The employee has been added successfully !");
                    this.setVisible(false);

                    try {
                        Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", this.entreprise);
                    } catch (IOException var13) {
                        var13.printStackTrace();
                    }
                }
            } else {
                DialogWindow.error(this, "First name incorrect or empty field !");
            }
        } else {
            DialogWindow.error(this, "Last name incorrect or empty field !");
        }

    }
}
