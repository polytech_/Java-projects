package ControllerEmpManagmentMainApp;

import Model.Company;
import Model.Department;
import Model.Employee;
import Model.Manager;
import ToolsIHM.DialogWindow;
import ToolsIHM.Serialization;
import ToolsIHM.TimeString;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class DeleteOrModifyEmployee extends JFrame implements ActionListener {
    private JButton bouttonSuppr;
    private JButton bouttonModif;
    private Employee employee;
    private Company entreprise;
    private JLabel prenom;
    private JTextField prenomSaisi;
    private JLabel textNom;
    private JTextField nomSaisi;
    private JLabel departement;
    private JComboBox departements;
    private JLabel heureArrivee;
    private JLabel heureDepart;
    private JComboBox heureArriveChoix;
    private JComboBox heureDepartChoix;
    private JComboBox comboBox;

    public DeleteOrModifyEmployee(Company entreprise, Employee emp) {
        this.setTitle("Delete/Modify an employee");
        this.entreprise = entreprise;
        this.employee = emp;
        this.initialize();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
    }

    private void initialize() {
        this.textNom = new JLabel("Last name :");
        this.textNom.setBounds(50, 30, 150, 20);
        this.nomSaisi = new JTextField(this.employee.getLastName());
        this.nomSaisi.setBounds(50, 50, 150, 20);
        this.nomSaisi.setEnabled(false);
        this.prenom = new JLabel("First name :");
        this.prenom.setBounds(50, 70, 150, 20);
        this.prenomSaisi = new JTextField(this.employee.getFirstName());
        this.prenomSaisi.setBounds(50, 90, 150, 20);
        this.prenomSaisi.setEnabled(false);
        this.departement = new JLabel("Department :");
        this.departement.setBounds(254, 50, 150, 20);
        this.departements = new JComboBox(this.entreprise.getDepartements());
        this.departements.setSelectedItem(this.employee.getDepartment().getName());
        if (this.employee instanceof Manager) {
            this.departements.setEnabled(false);
        }

        this.departements.setBounds(254, 70, 150, 20);
        this.heureArrivee = new JLabel("Arrival time:");
        this.heureArrivee.setBounds(50, 143, 150, 20);
        this.heureArriveChoix = new JComboBox(TimeString.affichageHeures());
        this.heureArriveChoix.setBounds(50, 163, 150, 20);
        this.heureArriveChoix.setSelectedItem(TimeString.formatString(this.employee.getarrivalTime()));
        this.heureDepart = new JLabel("Departure Time:");
        this.heureDepart.setBounds(254, 143, 150, 20);
        this.heureDepartChoix = new JComboBox(TimeString.affichageHeures());
        this.heureDepartChoix.setBounds(254, 163, 150, 20);
        this.heureDepartChoix.setSelectedItem(TimeString.formatString(this.employee.getdepartureTime()));
        this.bouttonSuppr = new JButton("Remove");
        this.bouttonSuppr.setBounds(315, 220, 109, 32);
        this.bouttonSuppr.addActionListener(this);
        this.bouttonSuppr.addActionListener((e) -> {
            if (DialogWindow.okCancel(this, "Are you sure about removing this employee ?", "Removing ...")) {
                this.entreprise.removeEmployee(this.employee);
                this.setVisible(false);
            }

        });
        if (this.employee instanceof Manager) {
            this.bouttonSuppr.setEnabled(false);
        }

        this.bouttonModif = new JButton("Modifier");
        this.bouttonModif.setBounds(50, 220, 109, 32);
        this.bouttonModif.addActionListener(this);
        this.getContentPane().add(this.bouttonSuppr);
        this.getContentPane().add(this.heureArrivee);
        this.getContentPane().add(this.bouttonModif);
        this.getContentPane().add(this.heureDepart);
        this.getContentPane().add(this.heureArriveChoix);
        this.getContentPane().add(this.heureDepartChoix);
        this.getContentPane().add(this.heureDepart);
        this.getContentPane().add(this.textNom);
        this.getContentPane().add(this.departement);
        this.getContentPane().add(this.prenom);
        this.getContentPane().add(this.departements);
        this.getContentPane().add(this.nomSaisi);
        this.getContentPane().add(this.prenomSaisi);
        this.setSize(461, 302);
        this.getContentPane().setLayout((LayoutManager)null);
        this.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        int choixHeureArrivee = this.heureArriveChoix.getSelectedIndex();
        int choixHeureDepart = this.heureDepartChoix.getSelectedIndex();

        try {
            String[] heuresArrivee = this.heureArriveChoix.getItemAt(choixHeureArrivee).toString().split(":");
            String[] heuresDepart = this.heureDepartChoix.getItemAt(choixHeureDepart).toString().split(":");
            LocalTime testHeureArrivee = LocalTime.of(Integer.parseInt(heuresArrivee[0]), Integer.parseInt(heuresArrivee[1]));
            LocalTime testHeureDepart = LocalTime.of(Integer.parseInt(heuresDepart[0]), Integer.parseInt(heuresDepart[1]));
            long DureedeTravail = ChronoUnit.MINUTES.between(testHeureArrivee, testHeureDepart);
            if (DureedeTravail < 0L) {
                DureedeTravail *= -1L;
            } else if (DureedeTravail > 720L) {
                DialogWindow.error(this, "The employee cannot work more than 12 hours a day !");
            } else {
                if (!(this.employee instanceof Manager)) {
                    this.employee.getDepartment().removeEmployee(this.employee);
                    Department newDpt = this.entreprise.getDepartment(this.entreprise.getDepartment(this.departements.getItemAt(this.departements.getSelectedIndex()).toString()).getName());
                    newDpt.addEmployee(this.employee);
                    this.employee.setDepartement(newDpt);
                }

                this.employee.setarrivalTime(testHeureArrivee);
                this.employee.setdepartureTime(testHeureDepart);
                DialogWindow.message(this, "Modification done successfully !");
                this.setVisible(false);

                try {
                    Serialization.save("./SerialLoad&Save/saveDonneesEntreprise.ser", this.entreprise);
                } catch (IOException var11) {
                    var11.printStackTrace();
                }
            }
        } catch (Exception var12) {
            System.out.println(var12);
        }

    }
}
